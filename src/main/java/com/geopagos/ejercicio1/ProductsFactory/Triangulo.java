package com.geopagos.ejercicio1.ProductsFactory;

import lombok.Builder;


public class Triangulo extends Figura implements IFigura {
    public Triangulo(Double base, Double altura) {
        super("TRIANGULO", base, altura, null);
    }


    @Override
    public String getTipoFigura() {
        return "TRIANGULO";
    }

    @Override
    public Double getSuperficie() {
        return ((this.base*this.altura)/2);
    }

    @Override
    public Double getBase() {
        return this.base;
    }

    @Override
    public Double getAltura() {
        return this.altura;
    }

    @Override
    public Double getDiametro() {
        return null;
    }
}
