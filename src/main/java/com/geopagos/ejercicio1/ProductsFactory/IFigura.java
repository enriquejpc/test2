package com.geopagos.ejercicio1.ProductsFactory;

public interface IFigura {

    public String getTipoFigura();

    public Double getSuperficie();

    public Double getBase();

    public Double getAltura();

    public Double getDiametro();

}
