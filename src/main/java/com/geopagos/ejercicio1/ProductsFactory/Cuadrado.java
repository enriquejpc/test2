package com.geopagos.ejercicio1.ProductsFactory;


public class Cuadrado extends Figura implements IFigura {
    public Cuadrado( Double base) {
        super("CUADRADO",  base, null, null);
    }

    @Override
    public String getTipoFigura() {
        return "CUADRADO";
    }

    @Override
    public Double getSuperficie() {
        return (Math.pow(base,2.0));
    }

    @Override
    public Double getBase() {
        return this.base;
    }

    @Override
    public Double getAltura() {
        return null;
    }

    @Override
    public Double getDiametro() {
        return null;
    }
}
